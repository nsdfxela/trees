﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trees
{
    class Program
    {
        static void Main(string[] args)
        {
            BinaryTree bt = new BinaryTree();
            /*Node root = new Node(8, "root");
            BinaryTree.Insert(root, new Node(3, "данные"));
            BinaryTree.Insert(root, new Node(1, "данные"));
            BinaryTree.Insert(root, new Node(6, "данные"));
            BinaryTree.Insert(root, new Node(4, "данные"));
            BinaryTree.Insert(root, new Node(7, "данные"));
            BinaryTree.Insert(root, new Node(13, "данные"));
            BinaryTree.Insert(root, new Node(14, "данные"));
            BinaryTree.Insert(root, new Node(10, "данные"));*/
            Node root = new Node(18, "root");
            bt.root = root;
            bt.Insert(root, new Node(12, "данные"));
            bt.Insert(root, new Node(4, "данные"));
            bt.Insert(root, new Node(15, "данные"));
            bt.Insert(root, new Node(13, "данные"));
            bt.Insert(root, new Node(17, "данные"));
            bt.Insert(root, new Node(14, "данные"));
            bt.Insert(root, new Node(1, "данные"));
            bt.Insert(root, new Node(3, "данные"));
            bt.Insert(root, new Node(2, "данные"));
            bt.Insert(root, new Node(25, "данные"));
            bt.Insert(root, new Node(28, "данные"));
            bt.Insert(root, new Node(29, "данные"));

            IVisualizer visualizer = new BinaryTreeVisualizer();
            visualizer.Visualise(@"D:\Stdy\trees\pictures\boo.bmp", bt);

            /*bt.InorderTraversal(root);
            Console.WriteLine();
            bt.PrefixTraversal(root);
            Console.WriteLine();
            bt.PostfixTraversal(root);*/
            Console.WriteLine("Поиск");
            bt.PrintNode(bt.Search(root, 7));
            bt.Next(bt.Search(root, 13));
            /*BinaryTree.Delete(root, BinaryTree.Search(root, 3));*/
            bt.Delete(root, bt.Search(root, 12));
        }
    }
}
