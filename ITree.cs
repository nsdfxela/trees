﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trees
{
    delegate void VisitNodeDelegate(Node n);

    interface ITree
    {
        void PrefixTraversal(Node n, VisitNodeDelegate ncb);
        void PrefixTraversal(Node n);
        void PostfixTraversal(Node n);
        void InorderTraversal(Node n);
        void VisitNode(Node n);
        void VisitNode(Node n, VisitNodeDelegate ncb);
        Node GetRoot();
        
    }
}
