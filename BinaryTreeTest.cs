﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;
namespace trees
{
    [TestFixture]
    class BinaryTreeTest
    {

        Node root;
        BinaryTree bt;
        [SetUp]
        public void initRoot()
        {
            root = new Node(8, "root");
            bt = new BinaryTree();
            bt.Insert(root, new Node(3, "данные"));
            bt.Insert(root, new Node(1, "данные"));
            bt.Insert(root, new Node(6, "данные"));
            bt.Insert(root, new Node(4, "данные"));
            bt.Insert(root, new Node(7, "данные"));
            bt.Insert(root, new Node(13, "данные"));
            bt.Insert(root, new Node(14, "данные"));
            bt.Insert(root, new Node(10, "данные"));
            
        }


        [Test]
        public void TestTree()
         {//http://neerc.ifmo.ru/wiki/index.php?title=%D0%94%D0%B5%D1%80%D0%B5%D0%B2%D0%BE_%D0%BF%D0%BE%D0%B8%D1%81%D0%BA%D0%B0,_%D0%BD%D0%B0%D0%B8%D0%B2%D0%BD%D0%B0%D1%8F_%D1%80%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F
             bt.Delete(root, bt.Search(root, 6));
             Assert.AreEqual(bt.Search(root, 3).right.key, 7);
             Assert.AreEqual(bt.Search(root, 3).right.left.key, 4);
             bt.Delete(root, bt.Search(root, 7));
             Assert.AreEqual(bt.Search(root, 3).left.key, 1);
        }

        [Test]
        public void searchTest()
        {
            Assert.AreEqual(bt.Search(root, 3).key, 3);
            Assert.AreEqual(bt.Search(root, 6).key, 6);

        }

    }
}
