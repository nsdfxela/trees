﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trees
{
    class Node
    {
        public object value;
        public int key;

        public float x;
        public float y;

        public Node left = null;
        public Node right = null;

        public Node parent = null;

        public Node Right{
            get{return right;}
            set {
                right = value;
                value.parent = this;
            }
        }
        public Node Left
        {
            get { return left; }
            set
            {
                left = value;
                value.parent = this;
            }
        }
        public Node(int key, object value)
        {
            this.key = key;
            this.value = value;

        }
        public override string ToString()
        {
            return key + " " + value.ToString();
        }
    }

}
