﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing ;
namespace trees
{
    class BinaryTreeVisualizer : IVisualizer
    {

        string filePath;
        Bitmap bitmap;
        Graphics graphics;
        

        int circleD = 60;
        float circleR;
        Font font;
        float circleX;
        float distance;
        float circleY;

        float shiftX;
        float shiftY;
        public void SetUp()
        {
            bitmap = new Bitmap(2000, 2000);
            circleR = circleD / 2;
            font = new Font("Arial", circleD / 3);
            circleX = bitmap.Width / 2;
            distance = circleD / 4;
            circleY = circleD + distance;

            shiftX = circleD + distance*2;
            shiftY = circleD + distance*5;
        }
        public void DrawNode(Node n)
        {
                if(n.parent==null)
                {
                    n.x = circleX;
                    n.y = circleY;
                }

                SolidBrush brush = new SolidBrush(Color.Black);
                SolidBrush whiteBrush = new SolidBrush(Color.White);
                Pen pen = new Pen(Color.Black);
                
                StringFormat formatter = new StringFormat();
                formatter.LineAlignment = StringAlignment.Center;
                formatter.Alignment = StringAlignment.Center;

              
                if (n.parent != null && n.parent.left == n)
                {
                    n.x = n.parent.x - shiftX;

                    
                }
                else if (n.parent != null)
                {
                    n.x = n.parent.x + shiftX;
                }

                if (n.parent != null)
                {
                    n.y = n.parent.y + shiftY;
                    graphics.DrawLine(pen, 
                        n.parent.x + distance * 2
                      , n.parent.y + distance * 4
                      , n.x + distance * 2
                      , n.y );
                }

                graphics.DrawEllipse(pen, n.x, n.y, circleD, circleD);
                graphics.DrawString(n.key.ToString(), font, brush, n.x+distance*2 , n.y+distance*2 , formatter);
        }
        
        public void Visualise(object args, ITree tree)
        {
            SetUp();
            //Bitmap bitmap = new Bitmap(args.ToString());
            this.filePath = args.ToString();

            using (graphics = Graphics.FromImage(bitmap))
            {
                tree.PrefixTraversal(tree.GetRoot(), DrawNode);
            }
            bitmap.Save(filePath);
            
        }
    }
}
