﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trees
{

    //между прочим на минуточку
    //http://cslibrary.stanford.edu/110/BinaryTrees.html#java
    class BinaryTree  : ITree
    {

        public Node root;
        public Node GetRoot()
        {
            return root;
        }
        public void InorderTraversal(Node n){
            if (n != null)
            {
                InorderTraversal(n.left);
                VisitNode(n);
                InorderTraversal(n.right);
            }


        }

        public void PrintNode(Node n)
        {
            if(n != null)
            Console.WriteLine(n.key + " " + n.value.ToString());
        }

        public void VisitNode(Node n)
        {
            PrintNode(n);                        
        }
        public void VisitNode(Node n, VisitNodeDelegate ncb)
        {
            ncb(n);
        }
        public void PrefixTraversal(Node n)
        {
            if (n != null)
            {
                VisitNode(n);
                PrefixTraversal(n.left);
                PrefixTraversal(n.right);
            }
        }
        public void PrefixTraversal(Node n, VisitNodeDelegate ncb)
        {
            if (n != null)
            {
                ncb(n);
                PrefixTraversal(n.left, ncb);
                PrefixTraversal(n.right, ncb);
            }
        }

        public void PostfixTraversal(Node n)
        {
            if (n != null)
            {
                PostfixTraversal(n.left);
                PostfixTraversal(n.right);
                VisitNode(n);
            }
        }
        /// <summary>
        /// Поиск элемента с заданным ключом
        /// </summary>
        /// <param name="n">корень поддерева</param>
        /// <param name="k">ключ</param>
        public Node Search(Node n, int k)
        {
            if (n == null || k == n.key)
            {
                return n;
            }
            if (k < n.key)
            {
                return Search(n.left, k);
            }
            else
            {
                return Search(n.right, k);
            }
        }

        public Node Min(Node n)
        {
            if (n.left == null)
                return n;
            return Min(n.left);
        }

        public Node Max(Node n)
        {
            if (n.right == null)
                return n;
            return Max(n.right);
        }

        public Node Next(Node n)
        {
            if (n.right != null)
                return Min(n.right);
           
           Node p = n.parent;
           while (p != null && n == p.right){
              n = p;
              p = p.parent;
           }
           return p;

        }

        public Node Previous(Node n)
        {
            if (n.left != null)
                return Max(n.left);

            Node p = n.parent;
            while (p != null && n == p.left)
            {
                n = p;
                p = p.parent;
            }
            return p;


        }

        /// <summary>
        /// Добавить узел к дереву
        /// </summary>
        /// <param name="n"></param>
        /// <param name="p"></param>
        public void Insert(Node n, Node p)
        {
            if (p.key > n.key)
            {
                if (n.right == null)
                { 
                  n.right = p;
                  p.parent = n;
                }

                else Insert(n.right, p);
            }
            else
            {
                if (n.left == null)
                {
                    n.left = p;
                    p.parent = n;
                }
                else Insert(n.left, p);
            }
        }

        //http://www.youtube.com/watch?v=V_3BM0ykITM#t=1896
        public void Delete(Node n, Node p)
        {
            if (p.parent == null) return;

            //оба сына удаляемого нулы
            if (p.left == null && p.right == null)
            {
                if (p.parent.left == p)
                    p.parent.left = null;
                else
                    p.parent.right = null;
                return;
            }
            //один сын удаляемого-нул
            if (p.left == null || p.right == null)
            {
                Node parentOfDeleting = p.parent;
                Node onlyChildOfDeleting = p.left ?? p.right;

                if (parentOfDeleting.left == p)
                {
                    parentOfDeleting.left = onlyChildOfDeleting;
                    onlyChildOfDeleting.parent = parentOfDeleting;
                }
                else
                {
                    parentOfDeleting.right = onlyChildOfDeleting;
                    onlyChildOfDeleting.parent = parentOfDeleting;
                }
            }
            //оба сына не нулы
            else
            {
                Node nextElement = Next(p);

                Delete(p, nextElement);
                /*if(nextElement.parent.left == nextElement)
                    nextElement.parent.left = null;
                else
                    nextElement.parent.right = null;*/

                p.key = nextElement.key;
                p.value = nextElement.value;                                                                
            }   


        }






    }
}
